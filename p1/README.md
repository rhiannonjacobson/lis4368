> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 Advanced Web Applications

## Rhiannon Jacobson

### Project #1 Requirements:

*Deliverables:*

1. Provide Bitbucket read-only access to lis4368 repo, include links to the other assignment repos you created in README.md, using Markdown syntax (README.md must also include screenshot as per above.)
2. Canvas links: lis4368 Bitbucket repo
3. *Note*: the carousel *must* contain (min 3.) slides that YOU created, that contain text and images that link to other content areas marketing/promoting your skills.

#### Project Screenshots:

*Screenshot LIS4368 Portal (Main/Splash Page)*:

![LIS 4368 Portal](img/portal.png)

*Screenshot Failed Validation*:

![Failed Validation](img/failed.png)

*Screenshot Passed Validation*:

![Passed Validation](img/passed.png)

#### Tutorial Links:

*Bitbucket Station Locations:*
[P1 Bitbucket Station Locations Link](https://bitbucket.org/rhiannonjacobson/bitbucketstationlocations/ "Bitbucket Station Locations")

*Bitbucket LIS 4368 Repo:*
[Bitbucket LIS4368 Link](https://bitbucket.org/rhiannonjacobson/lis4368/ "My LIS4368 repo")

*Local LIS4368 Web App:*
[Local LIS4368 Web App](http://localhost:9999/lis4368/ "Local LIS4368 Web App")
