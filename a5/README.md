> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 Advanced Web Applications

## Rhiannon Jacobson

### Assignment #5 Requirements:

*Deliverables:*

1. Provide Bitbucketread-only access to lis4368repo, includelinks to the other assignment repos you created in README.md, using Markdown syntax (README.mdmust also include screenshots as per above.)
2. Blackboard Links:lis4368 Bitbucket repo

#### README.md file should include the following items:

* Screenshot of Valid User Form Entry, Passed Validation and Associated Database Entry

#### Assignment Screenshots:

*Valid User Form Entry*:

![Valid User Form Entry](img/userform.png)

*Passed Validation*:

![Passed Validation](img/passed.png)

*Associated Database Entry*:

![Associated Database Entry](img/database.png)

#### Tutorial Links:

*Bitbucket Station Locations:*
[A5 Bitbucket Station Locations Link](https://bitbucket.org/rhiannonjacobson/bitbucketstationlocations/ "Bitbucket Station Locations")

*Bitbucket LIS 4368 Repo:*
[Bitbucket LIS4368 Link](https://bitbucket.org/rhiannonjacobson/lis4368/ "My LIS4368 repo")

*Local LIS4368 Web App:*
[Local LIS4368 Web App](http://localhost:9999/lis4368/ "Local LIS4368 Web App")
