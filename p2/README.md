> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 Advanced Web Applications

## Rhiannon Jacobson

### Project #2 Requirements:

*Deliverables:*

1. Provide Bitbucket read-only access to lis4368 repo, include links to the other assignment repos you created in README.md, using Markdown syntax (README.md must also include screenshot as per above.)
2. Canvas links: lis4368 Bitbucket repo
3. Screenshots 

#### Project Screenshots:

*Valid User Form Entry*:

![Valid User Form Entry](img/validuser.png)

*Passed Validation*:

![Passed Validation](img/passed.png)

*Display Data*:

![Display Data](img/display.png)

*Modify Form*:

![Modify Form](img/modify.png)

*Modified Data*:

![Modified Data](img/moddata.png)

*Delete Warning*:

![Delete Warning](img/delete.png)

*Associated Database Change*:

![Associated Database Changes](img/databasechanges.png)

#### Tutorial Links:

*Bitbucket Station Locations:*
[P2 Bitbucket Station Locations Link](https://bitbucket.org/rhiannonjacobson/bitbucketstationlocations/ "Bitbucket Station Locations")

*Bitbucket LIS 4368 Repo:*
[Bitbucket LIS4368 Link](https://bitbucket.org/rhiannonjacobson/lis4368/ "My LIS4368 repo")

*Local LIS4368 Web App:*
[Local LIS4368 Web App](http://localhost:9999/lis4368/ "Local LIS4368 Web App")
