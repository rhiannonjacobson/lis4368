import java.util.*;

public class ASCII {
	public static void main(String[] args) {
    	char[] charArray = new char[129];
    	int[] intArray = new int[129]; 
    	
    	char j = 0;
    	int z = 0;
    	int input = 0;
    	boolean flag = true;
    	
    	Scanner scnr = new Scanner(System.in);
    	
    	
    	for(int i = 0; i < 128; i++)
    	    {
    	        charArray[i] = j;
    	        intArray[i] = z;
    	        z++;
    	        j++;
    	    }
    	    
    	System.out.println("Printing characters A-Z as ASCII values: ");
    	
    	for(int i = 65; i < 91; i++)
    	    {
    	        System.out.println("Character " + charArray[i] + " has ascii value " + intArray[i]);
    	    }
    	    
    	System.out.println("\nPrinting ASCII values 48-122 as characters:");
    	
    	for(int i = 48; i < 123; i++)
    	    {
    	        System.out.println("ASCII value " + intArray[i] + " has a character value " + charArray[i]);
    	    }
    	    
    	 System.out.println("\nAllowing user ASCII value input:");
    	 

    	do{
    	    System.out.print("Please enter ASCII value (32 - 127): ");
    	    
    	    try {
    	     input = scnr.nextInt();
    	     if(input < 32 || input > 127)
    	        {
    	            System.out.print("ASCII value must be >= 32 and <= 127.\n\n");
    	            flag = false;
    	        }
    	        else
    	            flag = true;
    	        
    	 } catch(InputMismatchException exception) {
    	     System.out.println("Invalid integer--ASCII value must be a number.\n");
    	    scnr.next();
    	     flag = false;
    	 }
    	        
    	}while(flag == false);
    	 
    	
    	 System.out.println("\nASCII value " + intArray[input] + " has character value " + charArray[input]);
    	 
    	     
	}
}

