import java.util.*;

public class SwapNumber { 
    public static void main(String[] args)
    { 

        String s1= null, s2= null; 
        int num1 = 0, num2 = 0, x;  
        Scanner sc = new Scanner(System.in); 
        boolean flag1 = false, flag2 = false;
        boolean c1 = false, c2 = false;
        int count = 0;
        
        while(flag1 == false || flag2 == false)
       {
           try 
           { 
               if(flag1 == false)
                   {
                       
                       if(c1 == true)
                       {
                           System.out.print("Please try again. Enter first number: "); 
                       }else
                           System.out.print("Please enter first number: ");  
                       
                       s1 = sc.nextLine(); 
                       num1 = Integer.parseInt(s1);
                       flag1 = true;
                   }
             
                 if(flag2 == false) 
                 {	
                     if(c2 == true)
                     {
                           System.out.print("Please try again.Enter second number: ");
                     }else
                           System.out.print("\nPlease enter second number: ");  
                             count++;
                       
                        s2 = sc.nextLine();
                        num2 = Integer.parseInt(s2);
                        flag2 = true;
                 }
           }
           catch (Exception e) {
               c1 = true;
              if(count > 0)
              {
                  c2 = true;
              }
               System.out.println("Not valid integer \n");  
           }
       }
    
        System.out.println("");
        System.out.println("Before Swapping");
        System.out.println("num1 = " + num1);
        System.out.println("num2 = " + num2);
          
        x = num1;  
        num1 = num2;  
        num2 = x;  
        System.out.println("After Swapping");
        System.out.println("num1 = " + num1);
        System.out.println("num2 = " + num2);
    
    }
}