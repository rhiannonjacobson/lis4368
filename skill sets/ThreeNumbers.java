import java.util.Scanner;

class ThreeNumbers
{ 
    public static void main(String args[])
    {
        System.out.println("Program evaluates largest of three integers.");
        System.out.println("Note: Program checks for integers and non-numeric values.");
        System.out.println();

        int x, y, z;
        Scanner input = new Scanner(System.in); 

        System.out.print("Please enter first number: "); 
        while (!input.hasNextInt())
        { 
            System.out.println("Not valid integer!\n");
            input.next();
            System.out.print("Please try again. Enter first number: ");
        }

        x = input.nextInt(); 

        System.out.print("\nPlease enter second number: ");
        while (!input.hasNextInt())
        {
            System.out.println("Not valid integer!\n");
            input.next();
            System.out.print("Please try again. Enter second number: ");
        }

        y = input.nextInt();

        System.out.print("\nPlease enter third number: ");
        while (!input.hasNextInt())
        {
            System.out.println("Not valid integer!\n");
            input.next();
            System.out.print("Please try again. Enter third number: ");
        }

        z = input.nextInt();

        System.out.println();

        if (x > y && x > z)
        { 
            System.out.println("First number is the largest.");
        } 
        else if (y > z)
        {
            System.out.println("Second number is the largest.");
        }
        else 
        { 
            System.out.println("Third number is the largest.");
        }
       
        System.out.println();

    }
}