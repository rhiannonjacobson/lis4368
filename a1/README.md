# LIS 4368 - Advanced Web Applications

## Rhiannon Jacobson

### Assignment 1 Requirements:
- Demonstrate the ability to program and deploy client/server-side scripts
- Describe web-based input and output processes
- Demonstrate web application to data source connectivity
- Develop a dynamic web application using a mix of front-end and back-end web technologies.
- Employ OOP techniques, as well as business logic using a strongly typed language.
- Create client-and server-side data validation.

Three Ports: 

1. Distributed Version Control with Git and Bitbucket
2. Java/SP/Servlet Development Installation
3. Chapter Questions (Chs 1-4)

#### README.md file should include the following items:

* Screenshot of running java Hello;
* Screenshot of running http://localhost:9999 (#2 above, Step #4(b)in tutorial);
* git commands w/ short descriptions;
* Bitbucket repo links: a) this assignment and b) the completed tutorial about (bitbutcketstationlocations). 

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git into - creates new respository 
2. git status - displays the state of the working directory and staging area
3. git add - adds all modified and new files in the current directory to the staging area
4. git commit - saves all staged changes 
5. git push - update local repository to the newest commit
6. git pull - used to update the local version of a repository from a remote
7. git config - used to store a per-user configuration

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of Tomcat running http://localhost:9999*:

![Tomcat Installation Screenshot](img/tomcat.png)


#### Links:

*Bitbucket Station Locations:*
[A1 Bitbucket Station Locations Link](https://bitbucket.org/rhiannonjacobson/bitbucketstationlocations/ "Bitbucket Station Locations")

*Bitbucket LIS 4368 Repo:*
[Bitbucket LIS4368 Link](https://bitbucket.org/rhiannonjacobson/lis4368/ "My LIS4368 repo")

*Local LIS4368 Web App:*
[Local LIS4368 Web App](http://localhost:9999/lis4368/ "Local LIS4368 Web App")
