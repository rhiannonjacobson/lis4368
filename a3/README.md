> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 Advanced Web Applications

## Rhiannon Jacobson

### Assignment #3 Requirements:

*Deliverables:*

1. Entity Relationship Diagram (ERD)
2. Include data (at least 10 records each table)
3. Provide Bitbucket read-only access to repo (Language SQL) must include READ.md usin Markdown syntax, and include links to 
    - docs folder: a3.mwb and a3.sql
    - img folder: a3.png (export a3.mwb as a a3.png)
    - READ.me (MUST display a3.png ERD)
4. Blackboard Links: Bitbucket rep

#### README.md file should include the following items:

* Screenshot of ERD that links to the image

#### Assignment Screenshots:

*Screenshot A3 ERD*:

![A3 ERD](img/a3.png "ERD based upon A3 Requirements)

*A3 docs: a3.mwb and a3.sql*:

[A3 MWB File](docs/a3.mwb "A3 ERD in .mwb format")

[A3 SQL File](docs/a3.sql "A3 SQL Script")

#### Tutorial Links:

*Bitbucket Station Locations:*
[A3 Bitbucket Station Locations Link](https://bitbucket.org/rhiannonjacobson/bitbucketstationlocations/ "Bitbucket Station Locations")

*Bitbucket LIS 4368 Repo:*
[Bitbucket LIS4368 Link](https://bitbucket.org/rhiannonjacobson/lis4368/ "My LIS4368 repo")

*Local LIS4368 Web App:*
[Local LIS4368 Web App](http://localhost:9999/lis4368/ "Local LIS4368 Web App")
