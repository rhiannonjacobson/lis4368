# LIS4368 ADVANCED WEB APPLICATIONS DEVELOPMENT

## RHIANNON JACOBSON

### Class Number Requirements:
- Demonstrate the ability to program and deploy client/server-side scripts
- Describe web-based input and output processes
- Demonstrate web application to data source connectivity
- Develop a dynamic web application using a mix of front-end and back-end web technologies
- Employ OOP techniques, as well as business logic using a strongly typed language.
- Create client-and server-side data validation.

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Tomcat
    - Install AMPPS
    - Provide screenshots of installtions
    - Create Bitbucket repo
    - Complete Bitbucket tutorial
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Develop and deploy webapps
    - Write a welcome page
    - Write/complie servlet
    - Write Database servlet

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Entity Relationship Diagram (ERD)
    - Include data (at least 10 records each table)
    - Provide Bitbucket read-only access to repo (Language SQL) must include READ.md usin Markdown syntax, and include links to 
        -  docs folder: a3.mwb and a3.sql
        - img folder: a3.png (export a3.mwb as a a3.png)
        - READ.me (MUST display a3.png ERD)
    - Blackboard Links: Bitbucket rep

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Edit and compile CustomerServlet.java
    - Edit and complie Customer.java
    - Screenshots
        - A4 Failed Validation
        - A4 Passed Validation
    - Blackboard Links: Bitbucket repo

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Edit A5 page
    - Show data entered into database
    - Screenshots 
        - A5 Valid User Form Entry
        - A5 Passed Validation
        - A5 Associated database entry

6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Provide Bitbucket read-only access to lis4368 repo, include links to the other assignment repos you created in README.md, using Markdown syntax (README.md must also include screenshot as per above.)
    - Canvas links: lis4368 Bitbucket repo
    - *Note*: the carousel *must* contain (min 3.) slides that YOU created, that contain text and images that link to other content areas marketing/promoting your skills.

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Provide Bitbucket read-only access to lis4368 repo, include links to the other assignment repos you created in README.md, using Markdown syntax (README.md must also include screenshot as per above.)
    - Edit CustomerDB.java and CustomerServlet.java
    - Screenshots
        - P2 Valid User Form Entry
        - P2 Passed Validation
        - P2 Display Data
        - P2 Modify Form 
        - P2 Modified Data
        - P2 Delete Warning
        - P2 Associated database changes

8. git into - creates new respository 
9. git status - displays the state of the working directory and staging area
10. git add - adds all modified and new files in the current directory to the staging area
11. git commit - saves all staged changes 
12. git push - update local repository to the newest commit
13. git pull - used to update the local version of a repository from a remote
14. git config - used to store a per-user configuration

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/rhiannonjacobson/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")