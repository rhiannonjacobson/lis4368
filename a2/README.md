> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 Advanced Web Applications

## Rhiannon Jacobson

### Assignment #2 Requirements:

*Sub-Heading:*

1. Set up Development Environment
2. Start SQL Server
3. Compile Servlet files 
4. Assessment links

#### README.md file should include the following items:

* Assessment links
* Only one screenshot of the query results

#### Assignment Screenshots:

*Screenshot of Local Host Hello http://localhost:9999/hello*:

![Hello Screenshot](img/hello.png)

*Screenshot of Local Host Hello Home http://localhost:9999/hello/HelloHome.html*:

![Hello Home Screenshot](img/hellohome.png)

*Screenshot of Local Host Say Hello http://localhost:9999/hello/sayhello*:

![Say Hello Screenshot](img/sayhello.png)

*Screenshot of Local Host Querybook http://localhost:9999/hello/querybook.html*:

![Querybook Screenshot](img/querybook.png)

*Screenshot of Local Host Querybook Results http://http://localhost:9999/hello/query?author=Kumar*:

![Querybook Results Screenshot](img/querybookresults.png)

#### Tutorial Links:

*Bitbucket Station Locations:*
[A2 Bitbucket Station Locations Link](https://bitbucket.org/rhiannonjacobson/bitbucketstationlocations/ "Bitbucket Station Locations")

*Bitbucket LIS 4368 Repo:*
[Bitbucket LIS4368 Link](https://bitbucket.org/rhiannonjacobson/lis4368/ "My LIS4368 repo")

*Local LIS4368 Web App:*
[Local LIS4368 Web App](http://localhost:9999/lis4368/ "Local LIS4368 Web App")
